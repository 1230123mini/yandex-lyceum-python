n = int(input())
answer_set = set()
for i in range(n):
    m = int(input())
    k = int(input())
    string = input()
    string_inversed = string[m:12 * m:k]
    answer_set.add(string_inversed[::-1])
print(*answer_set, sep=' ')
