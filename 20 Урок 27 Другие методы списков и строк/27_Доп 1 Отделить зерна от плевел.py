words = input().split()
s_only_words = ''
s_only_symbols = ''
for word in range(len(words)):
    word_added = False
    symbol_added = False
    for letter in range(len(words[word])):
        if words[word][letter].isalpha():
            word_added = tuple
            s_only_words += words[word][letter]
        elif words[word][letter].isdigit():
            symbol_added = True
            s_only_symbols += words[word][letter]
        elif not words[word][letter].isalnum():
            symbol_added = True
            s_only_symbols += words[word][letter]
    if word_added:
        s_only_words += ' '
    if symbol_added:
        s_only_symbols += ' '
l_only_words = list()
for i in range(len(s_only_words.split())):
    l_only_words.append(s_only_words.split()[i].capitalize())
print(' '.join(l_only_words))
print(s_only_symbols)
