v_tea, v_cup, cups = int(input()), int(input()), int(input())
for cup in range(1, cups + 1):
    if v_tea - v_cup > 0 or v_tea - v_cup == 0:
        print('Чашечка', cup, 'полная. В чайнике осталось', str(v_tea - v_cup) + '.')
    elif v_tea - v_cup < 0 and v_tea > 0:
        print('Чашечка', cup, 'неполная. В чайнике осталось 0.')
    else:
        print('Чашечка', cup, 'пустая. В чайнике осталось 0.')
    v_tea -= v_cup