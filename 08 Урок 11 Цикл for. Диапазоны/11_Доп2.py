line = input()
s = k = 0
for letter in line:
    if letter == 'с':
        s += 1
    elif letter == 'к':
        k += 1
result = s * (-2) + k * 3 + 2
if result < 0:
    print('1/', 10 ** abs(result), sep='')
else:
    print(10 ** result)
