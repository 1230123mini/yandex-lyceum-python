day_number = int(input())
month = (day_number - 1) // 20 + 1
if day_number % 20 == 0:
    day = 19
else:
    day = day_number % 20 - 1
print(f'Месяц {month}, день {day}.')
