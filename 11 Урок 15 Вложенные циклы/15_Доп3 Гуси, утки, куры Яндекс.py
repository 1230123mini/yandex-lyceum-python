n = int(input())
s = int(input())
for goose in range(s // 25 + 1):
    for duck in range(s // 10 + 1):
        chicken = n - goose - duck
        if chicken >= 0 and goose + duck + chicken == n and \
                goose * 25 + duck * 10 + chicken * 7 == s:
            print(goose, duck, chicken)