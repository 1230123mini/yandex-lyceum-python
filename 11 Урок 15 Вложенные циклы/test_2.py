number_to_go = int(input())
for number in range(number_to_go + 1):
    del_count = 0
    del_is_simple = False
    answer_string = str(number) + ' = '
    for i in range(1, number + 1):
        if number % i == 0:
            del_count += 1
    if del_count <= 2:
        print(number, '=', number, '* 1')
    else:
        number_changed = number
        for delitel in range(2, number + 1):
            while number % delitel == 0:
                if number_changed == number:
                    answer_string += str(delitel)
                    number = number / delitel
                else:
                    answer_string = answer_string + ' * ' + str(delitel)
                    number = number / delitel
        print(answer_string)
