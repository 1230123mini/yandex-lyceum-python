symbol, n = input(), int(input())
for i in range(n):
    for j in range(n):
        if i == j or i == n - j - 1:
            print(symbol, end=' ')
        else:
            print(" ", end=' ')
    print()