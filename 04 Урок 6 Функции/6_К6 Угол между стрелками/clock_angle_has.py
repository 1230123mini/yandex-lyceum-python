h = int(input())  # hours 0..23
m = int(input())  # minutes 0..59
# h = 6
# m = 2

angle_minutes = m / 60 * 360
angle_hours = h % 12 / 12 * 360 + (360 / 12) * m / 60

result_angle = max(angle_hours, angle_minutes) - min(angle_hours, angle_minutes)

print(result_angle)