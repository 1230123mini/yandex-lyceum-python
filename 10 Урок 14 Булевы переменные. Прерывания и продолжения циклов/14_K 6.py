v, drop, fill_time, grad, total = int(input()), int(input()), 0, False, 0
while drop:
    if drop > 20:
        grad = True
    total += drop
    fill_time += 1
    if total >= v:
        break
    drop = int(input())
if total >= v:
    print('Время заполнения', fill_time, 'секунд.')
else:
    print('Ведро не полное.')
if grad:
    print('Град был!')
else:
    print('Града не было.')