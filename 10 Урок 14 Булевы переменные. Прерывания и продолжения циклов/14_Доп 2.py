width = int(input())
words = int(input())
tab = 0
direction_right = True
right_tab_count = 1
while words:
    s = input()
    if direction_right:
        if len(' ' * tab + s) == width:
            print(' ' * tab + s)
            direction_right = False
        elif len(' ' * tab + s) > width:  # проверили, что если строка выходит справа за пределы ширины
            tab = width - len(s)
            print(' ' * tab + s)
            direction_right = False
        else:
            print(' ' * tab + s)
            tab += 1  # обычное движение вправо
    else:
        # начали двигаться влево
        tab = width - len(s) - right_tab_count
        if len(' ' * tab + s) == width:
            print(' ' * tab + s)
            tab = 1
            direction_right = True
            right_tab_count = 1
        elif len(' ' * tab + s) > width:
            print(s)
            direction_right = True
            tab = right_tab_count = 1
        else:
            print(' ' * tab + s)
            right_tab_count += 1
        if tab <= 0:
            direction_right = True
            tab = right_tab_count = 1
    words -= 1
