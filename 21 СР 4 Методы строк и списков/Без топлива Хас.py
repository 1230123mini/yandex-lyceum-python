strings = list()
string = input()
s_index = 1
while 'star' not in string.lower():
    if s_index == 1:
        s = string.split()[0][0].upper() + string.split()[0][1:]  # Сделали большой первую букву в первом слове
        strings.append(s * len(s))
        s_index = 2
    elif s_index == 2:
        strings.append(string.replace(string[0], string[-1]))
        s_index = 3
    elif s_index == 3:
        string_list = list(string)
        for i in range(0, len(string_list), 4):
            string_list[i] = str(i)
        s = ''.join(string_list)
        strings.append(s)
        s_index = 4
    elif s_index == 4:
        strings.append(string)
        s_index = 1

    string = input()

for i in range(len(strings)):
    print(strings[i])
