template = input()
vowels = 'аяоёуюэеиы'
line = input()
no_words = True
while line:
    flag = True
    if '*' not in template:
        if len(line) != len(template):
            line = input()
            continue
        else:
            for i in range(len(line)):
                if template[i] == '1' and line[i] not in vowels or \
                        template[i] == '0' and line[i] in vowels or \
                        template[i] == '?':
                    continue
                else:
                    flag = False
                    break
    else:
        star_index = template.index('*')
        temp_start = template[:star_index]
        line_start = line[:star_index]
        temp_last = template[-1:star_index:-1]
        line_last = line[-1:len(line) - len(temp_last) - 1:-1] if \
            len(line) >= len(template) - 1 else line[-1]
        if len(temp_start) != len(line_start) or \
                len(temp_last) != len(line_last):
            line = input()
            continue
        else:
            for i in range(len(temp_start)):
                if temp_start[i] == '1' and line_start[i] not in vowels or \
                        temp_start[i] == '0' and line_start[i] in vowels or \
                        temp_start[i] == '?':
                    continue
                else:
                    flag = False
                    break
            for i in range(len(line_last)):
                if temp_last[i] == '1' and line_last[i] not in vowels or \
                        temp_last[i] == '0' and line_last[i] in vowels or \
                        temp_last[i] == '?':
                    continue
                else:
                    flag = False
                    break
    if flag:
        no_words = False
        print(line)
    line = input()
if no_words:
    print('Есть нечего, значить!')
