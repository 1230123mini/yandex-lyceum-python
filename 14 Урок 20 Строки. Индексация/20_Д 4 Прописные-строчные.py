char = input()
if 65 <= ord(char) <= 65 + 26:
    print(chr(ord(char) + 32))
elif 97 <= ord(char) <= 97 + 26:
    print(chr(ord(char) - 32))