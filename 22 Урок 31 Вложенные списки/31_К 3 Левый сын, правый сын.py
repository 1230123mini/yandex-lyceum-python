strings = [[int(i) for i in input().split()]]
next_string = list()
row = 0
while len(strings[-1]) != 1:
    if len(strings[row]) % 2 != 0:
        strings[row].append(0)
    for i in range(0, len(strings[row]), 2):
        next_string.append(strings[row][i] + strings[row][i + 1])
    strings.append(next_string[:])
    if strings[row][-1] == 0:
        del strings[row][-1]
    next_string.clear()
    row += 1
for i in range(len(strings) - 1, -1, -1):
    print(*strings[i], sep=' ')
