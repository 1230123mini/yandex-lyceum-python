string = input()
matrix = list()
string_count = 0
while string:
    matrix.append(string.split(' : '))
    string_count += 1
    string = input()

health = int(input())

for i in range(string_count):
    for j in range(len(matrix[0])):
        if int(matrix[i][j]) < health:
            print(0, end='\t')
        else:
            print(matrix[i][j], end='\t')
    print()
