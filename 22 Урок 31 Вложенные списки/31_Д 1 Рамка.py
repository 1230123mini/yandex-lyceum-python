rows = int(input())
matrix = list()
p_sum = 0
for i in range(rows):
    row = [int(el) for el in input().split()]
    matrix.append(row)

for row in range(rows):
    for col in range(len(matrix[0])):
        if row == 0 or row == rows - 1:
            p_sum += matrix[row][col]
        elif col == 0 or col == len(matrix[0]) - 1:
            p_sum += matrix[row][col]

for row in range(rows):
    for col in range(len(matrix[0])):
        if row == rows // 2 and col == len(matrix[0]) // 2:
            print(p_sum, end='\t')
        else:
            print(matrix[row][col], end='\t')
    print()
