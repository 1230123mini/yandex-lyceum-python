colors = list()
for i in range(int(input())):
    colors.append(input())
flags = int(input())
if (flags // len(colors)) != 0:
    print(*colors * (flags // len(colors)), sep='\n')
for i in range(0, flags % len(colors)):
    print(colors[i])
