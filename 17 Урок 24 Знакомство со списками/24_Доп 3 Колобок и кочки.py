n = int(input())
jumps = list()
jump_word = list()
answer = list()
index_of_letter = 0
possible = False
for i in range(n):
    jumps.append(int(input()))
for i in range(n):
    jump_word.append(input())

for i in range(n):
    word = jump_word[i]
    letters_set = set(word)
    letters_count_list = [0] * len(letters_set) * 2
    #  print(letters_set)
    #  print(letters_count_list)
    k = 0
    letter_amount = 0
    while len(letters_set) > 0:
        random_letter = letters_set.pop()
        #  print('random letter =', random_letter)
        letters_count_list[k] = (random_letter)
        for j in range(len(word)):
            if word[j] == random_letter:
                letter_amount += 1
        letters_count_list[k + 1] = letter_amount
        letter_amount = 0
        k += 2
    #  print(letters_count_list)
    if jumps[i] in letters_count_list:
        possible = True
        for index in range(len(letters_count_list)):
            if letters_count_list[index] == jumps[i]:
                index_of_letter = index - 1
        answer.append(letters_count_list[index_of_letter])
        letters_count_list.remove(letters_count_list[index])
        letters_count_list.remove(letters_count_list[index - 1])
        if jumps[i] in letters_count_list:
            possible = False
            break
    else:
        possible = False
        break
if not possible:
    print('нечленораздельно')
else:
    print(*answer, sep='')
