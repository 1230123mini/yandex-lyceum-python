Портняжка
У каждой профессии есть свои атрибуты. Когда художник рисует портного, обычно изображает его с большими ножницами и с портновской измерительной лентой.

Поработаем портным! Будем измерять все, что нам введут!

Напишите программу, которая считывает строки, пока не будет введена пустая строка, и выводит их длины.

Пример
Ввод        Вывод
There was a tailor in a German town.
His name was Hans.
All day long he sat on the table by the window, legs crossed, and sewed.
I sewed jackets, sewed trousers, sewed vests.

36
18
72
45