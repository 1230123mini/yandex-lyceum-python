tel_1 = int(input())
tel_2 = int(input())
count = 0
possible = False
while tel_1 > tel_2:
    count += 1
    if tel_1 / 2 == tel_2:
        possible = True
        break
    tel_1 /= 2
if possible:
    print(count)
elif tel_1 == tel_2:
    print(0)
else:
    print("IMPOSSIBLE")