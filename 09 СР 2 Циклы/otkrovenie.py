s, s_count, s_len = input(), 0, 0
while len(s) <= 30:
    if ('гном' in s or 'Гном' in s) and 'челове' not in s and 'люд' not in s:
        s_count += 1
        s_len += len(s)
    s = input()
if s_count > 0:
    print(s_len / s_count)
else:
    print(0)