jump_over = int(input())
steps = [i for i in range(1, int(input()) + 1)]
answer = []
for i in range(len(steps)):
    if steps[i] % jump_over == 0:
        answer.append('БОСИКОМ')
    else:
        answer.append(str(steps[i]))
print(', '.join(answer))
