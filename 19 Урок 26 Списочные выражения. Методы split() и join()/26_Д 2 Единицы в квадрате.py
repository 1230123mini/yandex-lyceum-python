number = int(input())
answer = []
n = '1'
while int(n) <= number:
    answer.append(str(int(n) ** 2))
    n = n + '1'
print(', '.join(answer))
