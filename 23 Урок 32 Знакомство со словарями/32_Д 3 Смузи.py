components = dict()

for i in range(5):
    component = input().split(': ')
    components[component[0]] = int(component[1])

# print(components)
# print(len(components))
for i in range(int(input())):
    order = input()
    if order == 'Сладкоежка':
        components['банан'] -= 1
        components['молоко'] -= 1
        components['мороженое'] -= 1
        if components['банан'] >= 0 and components['молоко'] >= 0 and components['мороженое'] >= 0:
            print(f'Пожалуйста, ваш {order}. Приятного аппетита!')
        else:
            print('Извините, не можем выполнить заказ.')
            components['банан'] += 1
            components['молоко'] += 1
            components['мороженое'] += 1

    if order == 'Клубнично-банановый':
        components['клубника'] -= 1
        components['молоко'] -= 1
        components['банан'] -= 1
        components['мороженое'] -= 1
        if components['клубника'] >= 0 and components['молоко'] >= 0 and components['банан'] >= 0 and components[
                'мороженое'] >= 0:
            print(f'Пожалуйста, ваш {order}. Приятного аппетита!')
        else:
            print('Извините, не можем выполнить заказ.')
            components['клубника'] += 1
            components['молоко'] += 1
            components['банан'] += 1
            components['мороженое'] += 1

    if order == 'Клубничный':
        components['клубника'] -= 1
        components['молоко'] -= 1
        components['мороженое'] -= 1
        if components['клубника'] >= 0 and components['молоко'] >= 0 and components['мороженое'] >= 0:
            print(f'Пожалуйста, ваш {order}. Приятного аппетита!')
        else:
            print('Извините, не можем выполнить заказ.')
            components['клубника'] += 1
            components['молоко'] += 1
            components['мороженое'] += 1

    if order == 'Хушаф':
        components['финики'] -= 1
        components['молоко'] -= 1
        if components['финики'] >= 0 and components['молоко'] >= 0:
            print(f'Пожалуйста, ваш {order}. Приятного аппетита!')
        else:
            print('Извините, не можем выполнить заказ.')
            components['финики'] += 1
            components['молоко'] += 1

    if order == 'Банановый':
        components['банан'] -= 1
        components['молоко'] -= 1
        components['финики'] -= 1
        if components['финики'] >= 0 and components['молоко'] >= 0 and components['банан'] >= 0:
            print(f'Пожалуйста, ваш {order}. Приятного аппетита!')
        else:
            print('Извините, не можем выполнить заказ.')
            components['банан'] += 1
            components['молоко'] += 1
            components['финики'] += 1
