dic = {}
n = int(input())
for _ in range(n):
    event, day, month = input().split(", ")
    dic[month] = dic.get(month, []) + [(int(day), event)]
request = input()
response = dic[request]
response.sort()
print(*[x[1] for x in response], sep='\n')