timetable = dict()
s = input()
bus_times_all = list()
max_time = 0
while s:
    cur_bus = int(s.split()[0])
    bus_time = int(s.split()[1].split(':')[0]) * 60 + int(s.split()[1].split(':')[1])

    if cur_bus not in timetable:
        #  print(f'автобус {cur_bus} впервые, время:{bus_time}')
        timetable[cur_bus] = [bus_time]
        bus_times_all.append(bus_time)
        timetable[cur_bus].sort()
        max_time = timetable[cur_bus]
    else:
        #  print(f'автобус {cur_bus} уже был, время:{bus_time}')
        timetable[cur_bus] += [bus_time]
        bus_times_all.append(bus_time)
        timetable[cur_bus].sort()
    #  print(f'timetable = {timetable}')
    s = input()

cur_time = input().split(':')
cur_time = int(cur_time[0]) * 60 + int(cur_time[1])
wanted_busses = [int(x) for x in input().split()]

# print(f'cur_time = {cur_time}')
# print(f'wanted_busses = {wanted_busses}')
# print(f'bus_times_all = {bus_times_all}')
# print(f'bus_times_all_max = {max(bus_times_all)}')
# print(f'timetale = {timetable}')

exit_time = max(bus_times_all)
min_exit_time = max(bus_times_all)

at_least_one_bus_found = False

for bus in wanted_busses:
    for bus_time in timetable[bus]:
        # print(f'cur_time + 6({cur_time + 6}), bus_time({bus_time}), {cur_time + 6} <= {bus_time}: {cur_time + 6 <= bus_time}')
        if cur_time + 6 <= bus_time:
            exit_time = bus_time - 6 - cur_time
            # print(f'подошедший автобус - {bus}')
            # print(f'подошедшее автобусное время - {bus_time}')
            # print(f'exit_time = {exit_time}')
            at_least_one_bus_found = True
            if exit_time < min_exit_time:
                min_exit_time = exit_time
                exit_time = min_exit_time
                # print(f'min_exit_time = {min_exit_time}')
            else:
                exit_time = min_exit_time

# print('\nответ')
if at_least_one_bus_found:
    print(exit_time)
else:
    print('None')
