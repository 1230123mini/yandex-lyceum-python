#!/usr/local/bin/python3.7
import sys
user_set, answer_set = set(), set()

# for i in open(sys.argv[2]): # каждую строку переносим как элемент множества с user_set
#     user_set.add(i.strip())
# for i in open(sys.argv[3]):
#     answer_set.add(i.strip())

user_answer_file = open(sys.argv[2])
correct_answer_file = open(sys.argv[3])

user_answer_list = user_answer_file.readline().split()
correct_answer_list = correct_answer_file.readline().split()

user_answer_set = set(user_answer_list)
correct_answer_set = set(correct_answer_list)

if user_answer_set == correct_answer_set:
    print('1 OK')
    sys.exit(0)
else:
    print('0 WA')
    print('Одидалось', answer_set)
    print('Вывелось', user_set)
    sys.exit(1)
